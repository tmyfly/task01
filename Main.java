import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
        String url = "https://www.opennet.ru/opennews/mini.shtml";
        String url2 = "https://mirknig.su";
        String dataPars = "<td class=tdate>02.09.2021";
        String dataPars2 = "Сегодня";

        PageParse pageParse = new PageParse(url);
        PageParse pageParse1 = new PageParse(url2);

        NewsCount newsCount = new NewsCount(pageParse.parsePage(), dataPars);
        NewsCount newsCount1 = new NewsCount(pageParse1.parsePage(), dataPars2);

        System.out.println("Find news \"opennet.ru\": "+ newsCount.countNews());
        System.out.println("Find news \"mirknig.su\": "+ newsCount1.countNews());
    }


    public static class NewsCount{
        private String strPars;
        private String dataPars;

        public NewsCount() {
        }

        public NewsCount(String strPars, String dataPars) {
            this.strPars = strPars;
            this.dataPars = dataPars;
        }

        public int countNews(){
            int count = 0;

            Pattern pattern = Pattern.compile(this.dataPars);
            Matcher matcher = pattern.matcher(this.strPars);
            while (matcher.find()) {
                count++;
            }
            return count;
        }
    }

    public static class PageParse{
        private String url;

        public PageParse() {
        }

        public PageParse(String url) {
            this.url = url;
        }

        public String parsePage(){
            String contentPage="";
            try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
                final HttpGet httpGet = new HttpGet(this.url);
                httpGet.setHeader("Accept", "application/xml");
                httpGet.setHeader("User-Agent",
                        "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");

                try (final CloseableHttpResponse response = httpclient.execute(httpGet)) {
                    contentPage = EntityUtils.toString(response.getEntity());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return contentPage;
        }
    }
}